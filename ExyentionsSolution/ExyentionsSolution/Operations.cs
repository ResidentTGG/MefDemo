﻿using System;
using System.ComponentModel.Composition;

namespace Extensions
{
    [Export(typeof(CalculatorSdk.IOperation))]
    [ExportMetadata("Symbol", '%')]
    public class Mod : CalculatorSdk.IOperation
    {
        public int Operate(int left, int right)
        {
            return left % right;
        }
    }

    [Export(typeof(CalculatorSdk.IOperation))]
    [ExportMetadata("Symbol", '+')]
    public class Add : CalculatorSdk.IOperation
    {
        public int Operate(int left, int right)
        {
            return left + right;
        }
    }

    [Export(typeof(CalculatorSdk.IOperation))]
    [ExportMetadata("Symbol", '-')]
    public class Subtract : CalculatorSdk.IOperation
    {

        public int Operate(int left, int right)
        {
            return left - right;
        }

    }
}
