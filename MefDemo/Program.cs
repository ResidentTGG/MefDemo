﻿using CalculatorSdk;
using System;

namespace MefDemo
{
    class Program
    {
        static void Main(string[] args)
        {    
            var sdk = new Sdk();

            String s;
            Console.WriteLine("Enter Command:");
            while (true)
            {
                s = Console.ReadLine();
                Console.WriteLine(sdk.Calculate(s));
            }
            
        }
    }
}
