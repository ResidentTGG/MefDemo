The purpose of MefDemo is to demonstrate the concepts and syntax of MEF, rather than to necessarily provide a realistic scenario for its use. <br>
Full example: https://docs.microsoft.com/en-us/dotnet/framework/mef/index in .net framework

Main app: MefDemo (.net core 2.0) <br>
SDK: CalculatorSdk (.net standard 2.0) <br>
Extensions: Extensions(.net standard 2.0) <br>
<br>
Extensions project has reference to SDK project, SDK project has reference to MefDemo. <br>

Extensions project build to netstandard2.0 folder <br>
WARNING: in this folder have to delete CalculatorSdk files