﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

namespace CalculatorSdk
{
    public interface ICalculator
    {
        String Calculate(String input);
    }

    public interface IOperation
    {
        int Operate(int left, int right);
    }

    public interface IOperationData
    {
        Char Symbol { get; }
    }

    public class Sdk
    {
        private CompositionContainer _container;

        [Import(typeof(ICalculator))]
        public ICalculator _calculator;

        public Sdk()
        {
            var catalog = new AggregateCatalog();

            catalog.Catalogs.Add(new AssemblyCatalog(typeof(Sdk).Assembly));
            catalog.Catalogs.Add(new DirectoryCatalog("X:/Learning/Extensions/netstandard2.0"));

            _container = new CompositionContainer(catalog);

            try
            {
                this._container.ComposeParts(this);
            }
            catch (CompositionException compositionException)
            {
                Console.WriteLine(compositionException.ToString());
            }
        }

        public string Calculate(string s)
        {
            return _calculator.Calculate(s);
        }

    }
}
